(function() {
	
	var app = angular.module('bookStore', ['ui.router']);

	app.config(function ($stateProvider){
	
		$stateProvider.state('listado', {
			url: '/listado',
			templateUrl: 'listado.html',
			controlller: 'booksController',

		});

		$stateProvider.state('ficha',{
			url: '/ficha/:id',
			templateUrl: 'ficha.html',
			controller: 'bookController'
		});

	});

	app.factory('Books', function(){
		var books = [
		{
			id: 1,
			titulo: "RESTful Web API Design with Node.js",
			autor: "Valentin Bojinov",
			editorial: "Packt Publishing",
			descripcion: "Si usted es un desarrollador web que quieran enriquecer sus habilidades de desarrollo para crear el lado del servidor, aplicaciones escalables, REST basados en la plataforma de Node.js, este libro es para ti. También es necesario tener en cuenta los conceptos de comunicación HTTP y debe tener un conocimiento práctico de JavaScript. El conocimiento del resto sería una ventaja añadida, pero definitivamente no es una necesidad",
			img: "img/node.jpg",
		},
		{
			id: 2,
			titulo: "El gran libro de Android, 2da Edición",
			autor: "Jesús Tomás Gironés",
			editorial: "Alfaomega",
			descripcion: "Este libro quiere ser una guía para aquellos lectores que pretendan introducirse en la programación de Android. Se ha estructurado en 11 capítulos que abordan aspectos específicos del desarrollo de aplicaciones. Resulta conveniente realizar una lectura secuencial de estos capítulos, dado que muchos de los conceptos que se abordan serán comprendidos mejor si hemos leído los capítulos anteriores.",
			img: "img/android.jpg",
		},
		{
			id: 3,
			titulo: "Por Un Scrum Popular: Notas para una Revolución Ágile",
			autor: "Tobias Mayer",
			editorial: "yoEditorial",
			descripcion: "The People's Scrum por Tobias Mayer , traducido y anotado por Alan Cyment.",
			img: "img/scrum.jpg",
		},
		{
			id: 4,
			titulo: "El Libro Negro del Programador: Cómo conseguir una carrera de éxito desarrollando software y cómo evitar los errores habituales",
			autor: "Rafael Gómez Blanes",
			editorial: "yoEditorial",
			descripcion: "El siglo XXI es el de la sociedad de la información y las nuevas tecnologías: todo ello no sería posible sin la enorme industria del software que le sirve de base. No obstante, los desarrolladores de software no aprovechan todas las oportunidades para desempeñar una carrera profesional de éxito, cometiendo siempre los mismos errores una y otra vez. Un buen proyecto software tiene que ver con habilidades creativas y artísticas más que aquellas necesariamente técnicas. El Libro Negro del Programador muestra qué distingue a un programador neófito de quien actúa y trabaja profesionalmente. En la era del emprendimiento y de la nueva economía, el desarrollo profesional de software es su pilar fundamental.",
			img: "img/negro.png",
		},
		{
			id: 5,
			titulo: "Programación de Shell Scripts",
			autor: " Alberto Luna Fernández y Pablo Sanz Mercado ",
			editorial: "UAM Ediciones",
			descripcion: "El presente libro ofrece una guía práctica para comenzar desde el principio, sin prácticamente conocimientos previos, a programar shell scripts. Esto constituye una herramienta de gran valía en la investigación pero también en el procesamiento de datos personales muy numerosos. Se incluyen ejemplos sencillos de programación que ilustran la gran potencia de dicho lenguaje y el gran apoyo que pueden representar en la investigación científica de cualquier disciplina.",
			img: "img/shell.jpg",
		},
	];
		return {
			getAllBooks: function (){
				return books;
			},
			getbookFromId: function (id){
				for(var i = 0; i < books.length; i++){
					if(books[i].id == id)
						return books[i];
				}
				return null;
			},
		};
	});

	app.controller('booksController',['Books', function (Books) {
			this.books = Books.getAllBooks();
	}]);

	app.controller('bookController', ['Books', '$state', '$stateParams', function (Books, $state, $stateParams){		console.log($stateParams.id);
		this.book = Books.getbookFromId($stateParams.id);
	}]);

})();